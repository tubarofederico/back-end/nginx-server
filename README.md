# Nginx Server

Dada la necesidad de migrar cada una de las aplicaciones que se encuentran dentro de la actual intranet desarrollada en un único sistema, se está implementando una arquitectura de aplicaciones descentralizados que se interconectaran por medio de un SSO que permitirá al usuario autenticar una única vez y así tener acceso a todas las aplicaciones a las cuales se encuentre autorizado a acceder.

Para lograr dicha migración se está construyendo una arquitectura de software libre sobre contenedores Linux en docker con Nginx como servidor web.

•	Nginx: Servidor web y proxy inverso. Esto permite tener múltiples aplicaciones bajo una misma IP gracias al proxy inverso que permite la resolución de la aplicación por nombre de DNS.

•	Keycloak: como SSO (Single Sign On) Proveedor de identidad descentralizado que consultara los usuarios sobre LDAP. La integración entre aplicaciones se realiza mediante el protocolo OpenID Connect.

•	PHP 8.2: Múltiples API’s REST FULL con Symfony 6.4 para proveer servicios a las aplicaciones de usuario (frontend). Dichas API’s utilizaran una conexión a bases de datos SQL Server.

•	Servidor Vue.js: Múltiples aplicaciones SPA (Single Page Aplicación) construidas con Vue.js como framework de desarrollo de interfaces de usuario, que facilita la creación de aplicaciones web reactivas, modulares y fáciles de mantener.
